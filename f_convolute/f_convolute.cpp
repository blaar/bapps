//
//  Created by Arnaud Blanchard on 22/12/15.
//  Copyright ETIS 2015. All rights reserved.
//
#include "blc_channel.h"
#include "blc_program.h"
#include <unistd.h>
#include <math.h>

#define DEFAULT_OUTPUT_NAME ":conv<pid>"

template <typename O_t, typename D, typename K> void convolute(O_t *output, int o_width, int o_height, D *input, int iwidth, int iheight, K* kernel, int kwidth, int kheight){
    int i, j, ki, kj;
    O_t value;
    
    BLC_COMMAND_LOOP(0){
        FOR(j, o_height){
            FOR(i, o_width){
                value=0;
                FOR(kj, kheight) {
                    FOR(ki, kwidth){
                        value+=input[i+ki+(kj+j)*iwidth]*kernel[kwidth-ki-1+(kheight-kj-1)*kwidth];                    }
                }
                output[i+j*o_width]=value;
            }
        }
    }
}

/**Convolute the image with the kerne. Work only on 2D for now.*/
int main(int argc, char **argv){
    blc_channel input, output, kernel;
    char const  *output_name, *input_name, *kernel_name;
    int output_width, output_height;
    int kwidth, kheight, iwidth, iheight;
    uint32_t output_type;
    
    blc_program_set_description("Compute convolution 2d of the data");
    blc_program_add_option(&output_name, 'o', "output", "blc_channel-out", "channel name", DEFAULT_OUTPUT_NAME);
    blc_program_add_parameter(&input_name, "blc_channel-in", 1, "data to convolute ", NULL);
    blc_program_add_parameter(&kernel_name, "blc_channel-in", 1, "kernel to use for convolution ", NULL);
    blc_program_init(&argc, &argv, blc_quit);
    blc_command_forward_blc_channels();

    input.open(input_name, BLC_CHANNEL_READ);
    if (input.dims_nb!=2) EXIT_ON_ARRAY_ERROR(&input, "It works only in 2D for now");
    iwidth=input.dims[0].length;
    iheight=input.dims[1].length;
    blc_loop_try_add_waiting_semaphore(input.sem_new_data);
    blc_loop_try_add_posting_semaphore(input.sem_ack_data);
    
    kernel.open(kernel_name, BLC_CHANNEL_READ);
    if (kernel.dims_nb!=2) EXIT_ON_ARRAY_ERROR(&kernel, "It works only in 2D for now");

    kwidth=kernel.dims[0].length;
    kheight=kernel.dims[1].length;
    
    if ((iwidth < kwidth) || (iheight < kheight)) EXIT_ON_ERROR("Kernel cannot be smaller than input");
    blc_loop_try_add_waiting_semaphore(kernel.sem_new_data);
    blc_loop_try_add_posting_semaphore(kernel.sem_ack_data);
    
    if (input.type=='FL32' ||  kernel.type=='FL32') output_type='FL32';
    else output_type='UIN8';
    
    if (strcmp(output_name, DEFAULT_OUTPUT_NAME)==0) SYSTEM_ERROR_CHECK(asprintf((char**)&output_name,":conv%d", getpid()), -1, NULL);
    
    output_width=iwidth-kwidth+1;
    output_height=iheight-kheight+1;
    output.create_or_open(output_name, BLC_CHANNEL_WRITE, output_type, input.format, 2, output_width, output_height);
    blc_loop_try_add_waiting_semaphore(output.sem_ack_data);
    blc_loop_try_add_posting_semaphore(output.sem_new_data);
    output.publish();

    switch (input.type){
        case 'UIN8':
            switch (kernel.type){
                case 'UIN8':
                    convolute(output.uchars, output_width, output_height, input.uchars, iwidth, iheight, kernel.uchars, kwidth, kheight);
                    break;
                case 'FL32':
                    convolute(output.floats, output_width, output_height, input.uchars, iwidth, iheight, kernel.floats, kwidth, kheight);
                    break;
                default:EXIT_ON_ARRAY_ERROR(&kernel, "Type kernel is not managed  for input 'UIN8', only UIN8|FL32 are");
                    
            }
            break;
        case 'FL32':
            switch (kernel.type){
                case 'UIN8':
                    convolute(output.floats, output_width, output_height, input.floats, iwidth, iheight, kernel.uchars, kwidth, kheight);
                    break;
                case 'FL32':
                    convolute(output.floats, output_width, output_height, input.floats, iwidth, iheight, kernel.floats, kwidth, kheight);
                    break;
                default:EXIT_ON_ARRAY_ERROR(&kernel, "Type kernel is not managed  for input 'FL32', only UIN8|FL32 are");
                    
            }
            break;
        default:EXIT_ON_ARRAY_ERROR(&input, "Type not managed, only UIN8|FL32 are");
    }
    return EXIT_SUCCESS;
}
