//
//  Created by Arnaud Blanchard on 22/12/15.
//  Copyright ETIS 2015. All rights reserved.
//
#include "blc_core.h"
#include "blc_channel.h"
#include "blc_program.h"
#include <unistd.h>
#include <math.h>

#define DEFAULT_OUTPUT_NAME ":oscillator<pid>"


int period;
float refresh_rate; //ms;
blc_channel output;
double step;
char const *display;
int display_height;
blc_mem graph_mem;
size_t i=0;
float gain=0;

int columns_nb=64;
int rows_nb=16;

static void start_float_loop(float frequency, float min, float max){
    int i;
    float time_gain, offset, gain;
    
    time_gain=frequency*2*M_PI/(float)output.total_length*refresh_rate/1000;
    if (output.sem_ack_data) refresh_rate=0; //We follow the rate of the reader and not our own rate
    
    gain=(max-min)/2.f; //amplitude sinus = 2
    offset=min+gain; //min sinus*gain = -gain
    
    BLC_COMMAND_LOOP(refresh_rate*1000){
        FOR(i, output.total_length){
            output.floats[i]=sin((blc_loop_iteration*output.total_length+i)*time_gain)*gain+offset;
        }

        if(display){
            blc_fprint_float_graph(stderr, output.floats, output.total_length, "oscillation", columns_nb, rows_nb, 1, 0, "Time", "Intensity");
            blc_eprint_cursor_up(rows_nb);
        }
    }
}

int main(int argc, char **argv){
    char const *refresh_rate_str, *channel_name, *buffer_length_str, *type_str, *frequency_str;
    char const *min_str, *max_str;
    float frequency, min, max;
    int buffer_length;
    uint32_t type;
    
    blc_program_add_option(&channel_name, 'o', "output", "blc_channel-in", "channel name", DEFAULT_OUTPUT_NAME);
    blc_program_add_option(&display, 'd', "display", NULL, "display value at each iteration", NULL);
    blc_program_add_option(&buffer_length_str, 's', "size", "integer", "buffer size (items nb)", "1");
    blc_program_add_option(&frequency_str, 'f', "frequency", "float", "frequency in Hz", "1");
    blc_program_add_option(&min_str, 'm', "min", "float", "minimum value of oscillation", "0");
    blc_program_add_option(&max_str, 'M', "max", "float", "maximum value of oscillation", "1");
    blc_program_add_option(&refresh_rate_str, 'r', "refresh", "real ", "refresh rate in ms", "10");
    blc_program_add_option(&type_str, 't', "type", "FL32", "type of data", "FL32");
    blc_program_init(&argc, &argv, blc_quit);
    blc_command_forward_blc_channels();

 //   period=strtol(period_str, NULL, 10);
    SSCANF(1, refresh_rate_str, "%f", &refresh_rate);
    step=(double)refresh_rate/(double)period;
    type=STRING_TO_UINT32(type_str);
    
    if (strcmp(DEFAULT_OUTPUT_NAME, channel_name)==0) asprintf((char**)&channel_name, ":oscillator%d",getpid());
    
    SSCANF(1, buffer_length_str, "%d", &buffer_length);
    SSCANF(1, frequency_str, "%f", &frequency);
    SSCANF(1, min_str, "%f", &min);
    SSCANF(1, max_str, "%f", &max);
    
    output.create_or_open(channel_name, BLC_CHANNEL_WRITE, type, 'NDEF', 1, buffer_length);
    output.publish();
    
    /*
    switch (output.type){
        case 'INT8': gain=INT8_MAX;break;
        case 'UIN8': gain=UINT8_MAX/2;break;
        default:EXIT_ON_CHANNEL_ERROR(&output, "Type not managed");
    }
    gain *= amp;*/
    /*
    blc_command_add("i", info_cb, NULL, "display value", NULL);
    blc_command_add("p", period_cb, "period(ms)", "signal period", NULL);
    blc_command_add("r", refresh_cb, "step(ms)", "time step", NULL);
    
    */

    blc_loop_try_add_waiting_semaphore(output.sem_ack_data);
    blc_loop_try_add_posting_semaphore(output.sem_new_data);
    
    start_float_loop(frequency, min, max);
    
    if (display) blc_eprint_cursor_down(rows_nb);

return EXIT_SUCCESS;
}
