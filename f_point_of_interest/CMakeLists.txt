# Set the minimum version of cmake required to build this project
cmake_minimum_required(VERSION 2.6)

# Set the name of the project as the directory basename
project(f_point_of_interest)

find_package(blc_channel)
find_package(blc_program)

add_definitions(${BL_DEFINITIONS})
include_directories(${BL_INCLUDE_DIRS})
add_executable(f_point_of_interest f_point_of_interest.cpp)
target_link_libraries(f_point_of_interest ${BL_LIBRARIES})




