#include "blc_channel.h"
#include "blc_program.h"
#include <unistd.h> //getpid
#include <sys/time.h>

static void discretize_floats(blc_array *output, blc_array const *input, int bins_nb,  float max=1.0, float min=0.0){
    int i, j, index;
   
    FOR(j, input->total_length) {
        index=(input->floats[j]-min)/(max-min)*bins_nb;
        if (index>=bins_nb) index=bins_nb-1;
        else if (index<0) index=0;
        
        FOR(i, bins_nb){
            output->floats[i+j*bins_nb] = (float)(i==index);
        }
    }
}

int main(int argc, char **argv){
    blc_array array;
    blc_channel input_channel, output_channel;
    char const  *output_channel_name, *input_channel_name, *period_str, *bins_nb_str, *display;
    float min=0, max=1;
    int64_t period;
    int iteration, initialized=0, bins_nb;
    
    blc_program_set_description("Discretize an analog signal in bins.");
    blc_program_add_option(&display, 'D', "display", NULL, "display a text graph with result", NULL);
    blc_program_add_option(&bins_nb_str, 'n', "bins_nb", "integer", "Number of bins where to distribute the discetize", "32");
    blc_program_add_option(&period_str, 'p', "period", "integer", "sampling period in ms", "10");
    blc_program_add_option(&output_channel_name, 'o', "output", "blc_channel", "name of the output channel", NULL);
    blc_program_add_parameter(&input_channel_name, "blc_channel", 1, "Input channel", NULL);
    blc_program_init(&argc, &argv, NULL);
    
    if (output_channel_name==NULL) SYSTEM_ERROR_CHECK(asprintf((char**)&output_channel_name, "/%s%d", blc_program_name, getpid()), -1, NULL); //This will not be freed
    
    input_channel.open(input_channel_name, BLC_CHANNEL_READ);
    if(input_channel.type!='FL32') EXIT_ON_CHANNEL_ERROR(&input_channel, "Only type FL32 is managed");
    
    period=strtod(period_str, NULL)*1000;
    bins_nb=strtod(bins_nb_str, NULL);

    if (input_channel.total_length==1)  output_channel.create_or_open(output_channel_name, BLC_CHANNEL_WRITE, 'FL32', 'NDEF', 1, bins_nb);
    else output_channel.create_or_open(output_channel_name, BLC_CHANNEL_WRITE, 'FL32', 'NDEF', 2, bins_nb, input_channel.total_length);

    output_channel.publish();
    
    iteration=0;
    BLC_COMMAND_LOOP(period){
        discretize_floats(&output_channel, &input_channel, bins_nb, max, min);
    }
    
    return EXIT_SUCCESS;
}
