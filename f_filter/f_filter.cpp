//
//  Created by Arnaud Blanchard on 22/12/15.
//  Copyright ETIS 2015. All rights reserved.
//
#include "blc_channel.h"
#include "blc_program.h"
#include <unistd.h>
#include <math.h>

#define DEFAULT_OUTPUT_NAME ":point_of_interest<pid>"

#define IN(x,y) input.uchars[(x)+(y)*in_width]

int main(int argc, char **argv){
	blc_channel input, output;
	char const  *output_name, *input_name;
	int out_width, out_height, in_width, in_height;
	int i, j, sobel;

	blc_program_set_description("Find a points of interest on a image");
	blc_program_add_option(&output_name, 'o', "output", "blc_channel-out", "channel name", DEFAULT_OUTPUT_NAME);
	blc_program_add_parameter(&input_name, "blc_channel-in", 1, "image from where you want to find the points of interest", NULL);
	blc_program_init(&argc, &argv, blc_quit);

	if (strcmp(output_name, DEFAULT_OUTPUT_NAME)==0) SYSTEM_ERROR_CHECK(asprintf((char**)&output_name,":point_of_interest%d", getpid()), -1, NULL);

	input.open(input_name, BLC_CHANNEL_READ);
	blc_loop_try_add_waiting_semaphore(input.sem_new_data);
	blc_loop_try_add_posting_semaphore(input.sem_ack_data);

	in_width=input.dims[0].length;
	in_height=input.dims[1].length;

	out_width=in_width-1;
	out_height=in_height-1;

	output.create_or_open(output_name, BLC_CHANNEL_WRITE, input.type, 'Y800', 2, out_width, out_height);
	output.publish();

	blc_loop_try_add_waiting_semaphore(output.sem_ack_data);
	blc_loop_try_add_posting_semaphore(output.sem_new_data);



	BLC_COMMAND_LOOP(0){
		FOR(j, out_height){
			FOR(i, out_width){
				output.uchars[i+j*out_width]=(IN(i, j)+IN(i+1, j)+IN(i+2,j)\
						+ IN(i, j+1)+IN(i+1, j+1)+IN(i+2,j+1)\
						+ IN(i, j+2)+IN(i+1, j+2)+IN(i+2,j+2))/9;
			}
		}
	}
	return EXIT_SUCCESS;
}
