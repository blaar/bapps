//
//  Created by Arnaud Blanchard on 22/12/15.
//  Copyright ETIS 2015. All rights reserved.
//
#include "blc_channel.h"
#include "blc_program.h"
#include <unistd.h>
#include <complex> //for std::abs

#define DEFAULT_OUTPUT_NAME ":norm<pid>"


template <typename T> void norm1(T *output,  T *input1, T *input2, int length ){
    int i;
    
    BLC_COMMAND_LOOP(0){
        FOR(i, length){
            output[i]=std::abs(input1[i])+std::abs(input2[i]);
        }
    }
}

void norm1(uchar *output,  uchar *input1, uchar *input2, int length ){
    int i;
    
    BLC_COMMAND_LOOP(0){
        FOR(i, length){
            output[i]=CLIP_UCHAR(input1[i]+input2[i]);
        }
    }
}

template <typename T> void norm2(T *output,  T *input1, T *input2, int length ){
    int i;
    
    BLC_COMMAND_LOOP(0){
        FOR(i, length){
            output[i]=sqrt(input1[i]*input1[i]+input2[i]*input2[i]);
        }
    }
}

template <typename T> void norm_sup(T *output,  T *input1, T *input2, int length ){
    int i;
    
    BLC_COMMAND_LOOP(0){
        FOR(i, length){
            output[i]=MAX(input1[i], input2[i]);
        }
    }
}

template <typename T> void norm(T *output,  T *input1, T *input2, int length, char const *norm_str ){
    if (strcmp(norm_str, "1")) norm1(output, input1, input2, length);
    else if (strcmp(norm_str, "2")) norm1(output, input1, input2, length);
    else if (strcmp(norm_str, "-1")) norm1(output, input1, input2, length);
    else EXIT_ON_ERROR("Norm '%s' is unknow", norm_str);
}


int main(int argc, char **argv){
    blc_channel input1, input2, output, kernel;
    char const  *output_name, *input1_name, *input2_name, *norm_str;
    int  width, height;
    
    blc_program_set_description("Compute convolution 2d of the data");
    blc_program_add_option(&output_name, 'o', "output", "blc_channel-out", "channel name", DEFAULT_OUTPUT_NAME);
    blc_program_add_option(&norm_str, 'L', "norm", "blc_channel-out", "espace de norm (1:manathan, 2:eucldean, -1:max) ", "2");

    blc_program_add_parameter(&input1_name, "blc_channel-in", 1, "element 1", NULL);
    blc_program_add_parameter(&input2_name, "blc_channel-in", 1, "element 2", NULL);
    blc_program_init(&argc, &argv, blc_quit);
    blc_command_forward_blc_channels();

    input1.open(input1_name, BLC_CHANNEL_READ);
    width=input1.dims[0].length;
    height=input1.dims[1].length;
    blc_loop_try_add_waiting_semaphore(input1.sem_new_data);
    blc_loop_try_add_posting_semaphore(input1.sem_ack_data);
    
    input2.open(input2_name, BLC_CHANNEL_READ);
    if (width!=input2.dims[0].length) EXIT_ON_ARRAY_ERROR(&input2, "The two inputs must have the same width of '%d'", width);
    if (height!=input2.dims[1].length) EXIT_ON_ARRAY_ERROR(&input2, "The two inputs must have the same height of '%d'", height);
    if (input1.type!=input2.type) EXIT_ON_ARRAY_ERROR(&input2, "The two inputs must have the same type");

    blc_loop_try_add_waiting_semaphore(input2.sem_new_data);
    blc_loop_try_add_posting_semaphore(input2.sem_ack_data);
    
    if (strcmp(output_name, DEFAULT_OUTPUT_NAME)==0) SYSTEM_ERROR_CHECK(asprintf((char**)&output_name,":norm%d", getpid()), -1, NULL);
    
        output.create_or_open(output_name, BLC_CHANNEL_WRITE, input1.type, input1.format, 2, width, height);
    blc_loop_try_add_waiting_semaphore(output.sem_ack_data);
    blc_loop_try_add_posting_semaphore(output.sem_new_data);
    output.publish();
    
    switch (input1.type){
        case 'UIN8': norm(output.uchars, input1.uchars, input2.uchars, output.total_length, norm_str);
            break;
        case 'FL32': norm(output.floats, input1.floats, input2.floats, output.total_length, norm_str);
            break;
        default:EXIT_ON_ARRAY_ERROR(&input1, "Type not managed, only UIN8|FL32 are");
    }
    return EXIT_SUCCESS;
}
