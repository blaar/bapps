//
//  Created by Arnaud Blanchard on 22/12/15.
//  Copyright ETIS 2015. All rights reserved.
//
#include "blc_channel.h"
#include "blc_program.h"
#include <unistd.h>
#include <complex> //for std::abs

#define DEFAULT_OUTPUT_NAME ":arg_max<pid>"

template <typename T> void local_max2D(uint16_t *coords, int maxes_nb, T *values, int width, int height, int offset_x, int offset_y ){
    int i, j, k, l, m;
    T value, *maxes;
    
    maxes=MANY_ALLOCATIONS(maxes_nb, T);
    
    BLC_COMMAND_LOOP(0){
        FOR(m, maxes_nb) {
            maxes[m]=0;
            coords[m*2]=0;
            coords[m*2+1]=0;
        }

        FOR(j, height-4){
            FOR(i, width-4){
                value=values[i+2+(j+2)*width];
                FOR(k, 5) {
                    FOR(l, 5) {
                        if (((k!=2) || (l!=2)) && (value < values[i+l+(j+k)*width])) break;
                    }
                    if (l!=5) break; //It has been broken
                }
                if ((l==5) && (k==5)) { //max local
                    FOR(m, maxes_nb) if (value>=maxes[m]) break; //order in max global
                    if (m!=maxes_nb) { //It has been broken before end
                        if (m!=maxes_nb-1) {
                            memmove(maxes+m+1, maxes+m, (maxes_nb-m-1)*sizeof(value));
                            memmove(coords+(m+1)*2, coords+m*2, (maxes_nb-m-1)*sizeof(uint16_t)*2);
                        }
                        maxes[m]=value;
                        coords[m*2]=i+2+offset_x;
                        coords[m*2+1]=j+2+offset_y;
                    }
                }
            }
        }
    }
}


int main(int argc, char **argv){
    blc_channel input, output, kernel;
    char const  *output_name, *input_name, *maxes_nb_str, *offset_x_str, *offset_y_str;
    int  width, height, maxes_nb, offset_x, offset_y;
    
    blc_program_set_description("Find arg of local max");
    blc_program_add_option(&maxes_nb_str, 'n', "max-nb", "integer", "Number of max", "1");
    blc_program_add_option(&output_name, 'o', "output", "blc_channel-out", "channel name", DEFAULT_OUTPUT_NAME);
    blc_program_add_option(&offset_x_str, 'x', "offset-x", "integer", "offset to x", "0");
    blc_program_add_option(&offset_y_str, 'y', "offset-y", "integer", "offset to y", "0");

    blc_program_add_parameter(&input_name, "blc_channel-in", 1, "element 2", NULL);
    blc_program_init(&argc, &argv, blc_quit);
    blc_command_forward_blc_channels();
 
    SSCANF(1, maxes_nb_str, "%d", &maxes_nb);
    SSCANF(1, offset_x_str, "%d", &offset_x);
    SSCANF(1, offset_y_str, "%d", &offset_y);
    
    input.open(input_name, BLC_CHANNEL_READ);
    if (input.dims_nb!=2) EXIT_ON_ARRAY_ERROR(&input, "Only 2 dims are managed for now");
    width=input.dims[0].length;
    height=input.dims[1].length;
    blc_loop_try_add_waiting_semaphore(input.sem_new_data);
    blc_loop_try_add_posting_semaphore(input.sem_ack_data);
    
    if (strcmp(output_name, DEFAULT_OUTPUT_NAME)==0) SYSTEM_ERROR_CHECK(asprintf((char**)&output_name,":arg_max%d", getpid()), -1, NULL);
    
    output.create_or_open(output_name, BLC_CHANNEL_WRITE, 'UI16', 'NDEF', 2, maxes_nb, input.dims_nb);
    blc_loop_try_add_waiting_semaphore(output.sem_ack_data);
    blc_loop_try_add_posting_semaphore(output.sem_new_data);
    output.publish();
    
    switch (input.type){
        case 'UIN8': local_max2D(output.uints16, maxes_nb, input.uchars, width, height, offset_x, offset_y);
            break;
        case 'FL32': local_max2D(output.uints16, maxes_nb, input.floats, width, height, offset_x, offset_y);
            break;
        default:EXIT_ON_ARRAY_ERROR(&input, "input type not managed, only UIN8|FL32 are");
    }
    return EXIT_SUCCESS;
}
