//
//  Created by Arnaud Blanchard on 22/12/15.
//  Copyright ETIS 2015. All rights reserved.
//
#include "blc_channel.h"
#include "blc_program.h"
#include <unistd.h>
#include <math.h>

#define DEFAULT_OUTPUT_NAME ":argmax<pid>"


static float find_float_max(float *floats, int floats_nb, float const *find_arg=NULL){
    float max;
    int i, imax;
    max=floats[0];
    imax=0;
    for(i=1; i!=floats_nb; i++){
        if(floats[i] > max){
            max=floats[i];
            imax=i;
        }
    }
    if (find_arg) return (imax+0.5f)*(*find_arg);
    else return max;
}

static void start_channel_float_loop(blc_channel const *input, blc_channel *output, float const *find_arg=NULL)
{
    BLC_COMMAND_LOOP(0){
        output->floats[0]=find_float_max(input->floats, input->total_length, find_arg);
    }
}

static void start_print_float_loop(blc_channel const *input, float const* find_arg=NULL){
    float imax;
    BLC_COMMAND_LOOP(0){
        imax=find_float_max(input->floats, input->total_length, find_arg);
        printf("%f\n", imax);
        if( blc_output_terminal) blc_eprint_cursor_up(1);
    }
    if( blc_output_terminal) blc_eprint_cursor_down(1);
}

int main(int argc, char **argv){
    blc_channel input, output;
    char const  *output_name, *input_name, *find_arg_str;
    float *find_arg_gain=NULL;
    float arg_gain;
    
    blc_program_set_description("Find the value of argument of the maximal value");
    blc_program_add_option(&find_arg_str, 'a', "find_arg", "real", "Return argmax mutiplied by the real instead of max", NULL);
    blc_program_add_option(&output_name, 'o', "output", "blc_channel-out", "channel name", DEFAULT_OUTPUT_NAME);
    blc_program_add_parameter(&input_name, "blc_channel-in", 1, "channel you want to find max", NULL);
    blc_program_init(&argc, &argv, blc_quit);
    blc_command_forward_blc_channels();

    input.open(input_name, BLC_CHANNEL_READ);
    
    blc_loop_try_add_waiting_semaphore(input.sem_new_data);
    blc_loop_try_add_posting_semaphore(input.sem_ack_data);
    
    if (find_arg_str){
        SSCANF(1, find_arg_str, "%f", &arg_gain);
        find_arg_gain=&arg_gain;
        if(strcmp(output_name, DEFAULT_OUTPUT_NAME)==0) asprintf((char**)&output_name, ":imax%d", getpid());
    }
    else if(strcmp(output_name, DEFAULT_OUTPUT_NAME)==0) asprintf((char**)&output_name, ":max%d", getpid());

    
    //The output is on terminal
    if (strcmp(output_name, "-")==0){
        switch (input.type){
            case 'FL32':start_print_float_loop(&input, find_arg_gain);
                break;
            default:EXIT_ON_CHANNEL_ERROR(&input, "Type is not managed. Only 'FL32' is.");
                break;
        }
    }
    else
    {
        output.create_or_open(output_name, BLC_CHANNEL_WRITE, input.type, 'NDEF', 1, 1);
        output.publish();
        
        blc_loop_try_add_waiting_semaphore(output.sem_ack_data);
        blc_loop_try_add_posting_semaphore(output.sem_new_data);

        switch (input.type){
            case 'FL32':start_channel_float_loop(&input, &output, find_arg_gain);
                break;
            default:EXIT_ON_CHANNEL_ERROR(&input, "Type is not managed. Only 'FL32' is.");
                break;
        }
    }
    
    return EXIT_SUCCESS;
}
